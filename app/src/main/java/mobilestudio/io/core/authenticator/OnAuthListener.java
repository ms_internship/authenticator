package mobilestudio.io.core.authenticator;

import mobilestudio.io.core.authenticator.model.User;

/**
 * Created by pisoo on 9/18/2017.
 */

public interface OnAuthListener {
    void onSuccess (User user);
    void onFailed (String message );
}
