package mobilestudio.io.core.authenticator.datahandle;

import mobilestudio.io.core.authenticator.model.User;

/**
 * Created by Sayed on 9/18/2017.
 */

public interface OnDataFetched {

    void onSuccess(User user);
}
