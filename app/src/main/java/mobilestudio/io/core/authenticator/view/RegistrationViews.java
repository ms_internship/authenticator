package mobilestudio.io.core.authenticator.view;

import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import mobilestudio.io.core.authenticator.signup.RegisterView;

/**
 * Created by pisoo on 9/13/2017.
 */

public  class RegistrationViews {
  public  RegisterView<EditText> firstName , lastName , email , password , confirmPassword, phone , birthdate  ;
   public  RegisterView<Button> signup ;
    public RegisterView<Spinner> gender ;

    public RegistrationViews() {
        firstName = new RegisterView<>();
        lastName = new RegisterView<>() ;
        email = new RegisterView<>();
        password = new RegisterView<>();
        confirmPassword = new RegisterView<>();
        phone = new RegisterView<>() ;
        birthdate = new RegisterView<>();
        signup = new RegisterView<>() ;
        gender = new RegisterView<>();

    }
}
