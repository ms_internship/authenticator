package mobilestudio.io.core.authenticator.signup;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.auth.PhoneAuthCredential;

import mobilestudio.io.core.authenticator.AuthenticationManager;
import mobilestudio.io.core.authenticator.authenticate.Authenticator;
import mobilestudio.io.core.authenticator.authenticate.OnAuthenticateCallback;
import mobilestudio.io.core.authenticator.verification.OnVerificationCallback;
import mobilestudio.io.core.authenticator.verification.Verifier;

/**
 * Created by pisoo on 9/19/2017.
 */

public class PhoneNumberSignUpAuth extends BaseSignUpAuth {

    private EditText phoneNumber;
    private AuthenticationManager manager;
    private Verifier verifier;

    public PhoneNumberSignUpAuth(final Authenticator authenticator, final AuthenticationManager manager, Verifier verifier, final EditText phoneNumber ) {
        super(authenticator);
        this.phoneNumber = phoneNumber;
        this.manager = manager;
        this.verifier = verifier;
    }
    @Override
    public void signUp(final onSignUpCallback callback){
        final String phone = phoneNumber.getText().toString();
        verifier.verify(phone, new OnVerificationCallback() {
            @Override
            public void onSuccess(PhoneAuthCredential credential) {
                authenticator.loginWithPhoneNumber(credential, new OnAuthenticateCallback() {
                    @Override
                    public void onSuccess(String UID) {
                        if(callback != null){
                            callback.onSuccess();
                        }
                        manager.onSuccessNormal(UID);
                    }

                    @Override
                    public void onFailed(String errorMessage) {
                        manager.onFail(errorMessage);
                        if (errorMessage.equals("The sms verification code used to create the phone auth credential is invalid." +
                                " Please resend the verification code sms and be sure use the verification code provided by the user")) {
                            verifier.resendCode();
                        }
                    }
                });
            }

            @Override
            public void onFailed(String message) {
                manager.onFail(message);
            }
        });
    }



}
