package mobilestudio.io.core.authenticator.login;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.google.firebase.auth.PhoneAuthCredential;

import mobilestudio.io.core.authenticator.AuthenticationManager;
import mobilestudio.io.core.authenticator.R;
import mobilestudio.io.core.authenticator.authenticate.Authenticator;
import mobilestudio.io.core.authenticator.authenticate.OnAuthenticateCallback;
import mobilestudio.io.core.authenticator.datahandle.Navigator;
import mobilestudio.io.core.authenticator.datahandle.PhoneNumberDataHandle;
import mobilestudio.io.core.authenticator.verification.OnVerificationCallback;
import mobilestudio.io.core.authenticator.verification.FirebasePhoneNumberVerifier;


/**
 * Created by pisoo on 9/11/2017.
 */

public class PhoneNumberLoginAuth extends LoginAuthCallback {

    private EditText phoneNumber;
    private AuthenticationManager manager;
    private Navigator firstTimeHandler;
    private FirebasePhoneNumberVerifier verifier;

    public PhoneNumberLoginAuth(final Authenticator authenticator, final AuthenticationManager manager, FirebasePhoneNumberVerifier verifier, final Navigator firstTimeHandler, final Activity activity, final EditText phoneNumber, Button getVerifyButton) {
        super(authenticator, activity, firstTimeHandler);
        this.phoneNumber = phoneNumber;
        this.firstTimeHandler = firstTimeHandler;
        this.manager = manager;
        this.verifier = verifier;
        getVerifyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                login();
            }
        });
    }

    @Override
    public void login() {
        final String phone = phoneNumber.getText().toString();
        verifier.verify(phone, new OnVerificationCallback() {
            @Override
            public void onSuccess(PhoneAuthCredential credential) {
                firstTimeHandler.setDataHandler(new PhoneNumberDataHandle(phone));
                manager.setNavigator(firstTimeHandler);

                authenticator.loginWithPhoneNumber(credential, new OnAuthenticateCallback() {
                    @Override
                    public void onSuccess(String UID) {
                        manager.onSocialSuccess(UID);
                    }

                    @Override
                    public void onFailed(String errorMessage) {
                        manager.onFail(errorMessage);
                        Log.v("verify Failed ", errorMessage);
                        if (errorMessage.equals(getActivity().getString(R.string.invalid_code))) {
                            verifier.resendCode();
                        }
                    }
                });
            }

            @Override
            public void onFailed(String message) {
                manager.onFail(message);
            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

    }
}


