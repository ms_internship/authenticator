package mobilestudio.io.core.authenticator.login;

import android.app.Activity;
import android.content.Intent;

import mobilestudio.io.core.authenticator.authenticate.Authenticator;
import mobilestudio.io.core.authenticator.datahandle.Navigator;

/**
 * Created by Sayed on 9/7/2017.
 */

public abstract class LoginAuthCallback extends BaseLoginAuth {
    public Navigator navigator;

    public LoginAuthCallback(Authenticator authenticator, Activity activity , Navigator firstTimeHandler) {
        super(authenticator, activity);
        this.navigator = firstTimeHandler;
    }

   public abstract void onActivityResult(int requestCode, int resultCode, Intent data) ;
}
