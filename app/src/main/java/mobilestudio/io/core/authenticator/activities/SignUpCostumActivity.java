package mobilestudio.io.core.authenticator.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;

import mobilestudio.io.core.authenticator.OnAuthListener;
import mobilestudio.io.core.authenticator.R;
import mobilestudio.io.core.authenticator.builder.SignUpAuthBuilder;
import mobilestudio.io.core.authenticator.authenticate.FirebaseAuthenticator;
import mobilestudio.io.core.authenticator.authenticate.OnAuthenticateCallback;
import mobilestudio.io.core.authenticator.datahandle.FirebaseDBHandler;
import mobilestudio.io.core.authenticator.model.User;
import mobilestudio.io.core.authenticator.verification.FirebasePhoneNumberVerifier;

import static mobilestudio.io.core.authenticator.R.id.bt_signUp;
import static mobilestudio.io.core.authenticator.R.id.ed_birthDatee;
import static mobilestudio.io.core.authenticator.R.id.ed_phone;

public class SignUpCostumActivity extends AppCompatActivity  implements OnAuthListener {
    EditText email , password , confrimPassword , phone , birthdate   ;
    Button signupButton ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_costum);
        email = (EditText) findViewById(R.id.ed_EmailAddress);
        password = (EditText) findViewById(R.id.ed_password );
        confrimPassword = (EditText) findViewById(R.id.ed_confrimPassword);
        signupButton = (Button) findViewById(bt_signUp);
        phone = (EditText) findViewById(ed_phone);
        birthdate = (EditText) findViewById(ed_birthDatee);
        new SignUpAuthBuilder(new FirebaseAuthenticator(),new FirebaseDBHandler("UserProfile")
                ,new FirebasePhoneNumberVerifier(this),this)
                        .onEmailAndPasswordView(email,password,confrimPassword,signupButton,true)
                        .onPhoneView(phone,false,true)
                        .onBirthDateView(birthdate, true , this)
                        .build();
    }

    @Override
    public void onSuccess(User user) {
        startActivity( new Intent(this , MainActivity.class));

    }

    @Override
    public void onFailed(String errorMessage) {
    }
}
