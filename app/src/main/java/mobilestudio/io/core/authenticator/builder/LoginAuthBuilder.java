package mobilestudio.io.core.authenticator.builder;

import android.app.Activity;
import android.content.Intent;
import android.widget.Button;
import android.widget.EditText;

import java.util.ArrayList;
import java.util.List;

import mobilestudio.io.core.authenticator.AuthenticationManager;
import mobilestudio.io.core.authenticator.OnAuthListener;
import mobilestudio.io.core.authenticator.authenticate.Authenticator;
import mobilestudio.io.core.authenticator.datahandle.IDBHandler;
import mobilestudio.io.core.authenticator.datahandle.UserDataProvider;
import mobilestudio.io.core.authenticator.login.PhoneNumberLoginAuth;
import mobilestudio.io.core.authenticator.verification.FirebasePhoneNumberVerifier;
import mobilestudio.io.core.authenticator.view.DefaultLoginView;
import mobilestudio.io.core.authenticator.datahandle.Navigator;
import mobilestudio.io.core.authenticator.login.LoginAuthCallback;
import mobilestudio.io.core.authenticator.login.EmailAndPasswordLoginAuth;
import mobilestudio.io.core.authenticator.login.FacebookLoginAuth;
import mobilestudio.io.core.authenticator.login.GmailLoginAuth;

/**
 * Created by pisoo on 9/7/2017.
 */

public class LoginAuthBuilder {

    private List<LoginAuthCallback> authenticators = new ArrayList<>();
    private Authenticator authenticator;
    private DefaultLoginView loginView;
    private Activity loginActivity;
    private Navigator firstTimeHandler;
    private AuthenticationManager manager;

    public LoginAuthBuilder(Authenticator authenticator, IDBHandler dbHandler, OnAuthListener authListener, Activity login, Class<?> register) {
        this.loginActivity = login;
        this.authenticator = authenticator;
        firstTimeHandler = new Navigator(login, register);
        manager = AuthenticationManager.getInstance(UserDataProvider.getInstance(dbHandler), dbHandler);
        manager.setOnSocial(register != null);
        manager.setListener(authListener);
    }

   public LoginAuthBuilder onEmailAndPasswordLogin(EditText email, EditText password, Button login) {
        authenticators.add(new EmailAndPasswordLoginAuth(authenticator, manager, email, password, login, loginActivity));
        return this;
    }

    public LoginAuthBuilder onFacebookLogin(Button login) {
        authenticators.add(new FacebookLoginAuth(authenticator, manager, firstTimeHandler, login, loginActivity));
        return this;
    }

    public LoginAuthBuilder onGmailLogin(Button login, String gmailClientID) {
        authenticators.add(new GmailLoginAuth(authenticator, manager, firstTimeHandler, login, gmailClientID, loginActivity));
        return this;
    }

    public LoginAuthBuilder onPhoneNumber(EditText number, Button sendCode) {
        authenticators.add(new PhoneNumberLoginAuth(authenticator, manager,new FirebasePhoneNumberVerifier(loginActivity), firstTimeHandler, loginActivity, number, sendCode));
        return this;
    }

    public LoginAuthBuilder inflateDefaultView(Activity context, int id) {
        loginView = context.findViewById(id);
        return this;
    }

    public LoginAuthBuilder onDefaultEmailAndPasswordLogin() {
        loginView.enableEmail();
        return onEmailAndPasswordLogin(loginView.getEmail(), loginView.getPassword(), loginView.getEmailLoginButton());
    }

    public LoginAuthBuilder onDefaultFacebookLogin() {
        loginView.enableFacebook();
        return onFacebookLogin(loginView.getFacebookLoginButton());
    }

    public LoginAuthBuilder onDefaultGmailLogin(String gmailClientID) {
        loginView.enableGmail();
        return onGmailLogin(loginView.getGmaiLoginButton(), gmailClientID);
    }

    public LoginAuthBuilder onDefaultPhoneNumberLogin() {
        loginView.enablePhoneNumber();
        return onPhoneNumber(loginView.getPhoneNumber(), loginView.getSentVerificationButton());
    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        for (LoginAuthCallback loginAuthCallback : authenticators) {
            loginAuthCallback.onActivityResult(requestCode, resultCode, data);
        }
    }

    public LoginAuthController build() {
        return new LoginAuthController(this);
    }
}
