package mobilestudio.io.core.authenticator.verification;

import com.google.firebase.auth.PhoneAuthCredential;

/**
 * Created by pisoo on 9/19/2017.
 */

public interface OnVerificationCallback {
    void onSuccess(PhoneAuthCredential credential);

    void onFailed(String message);
}
