package mobilestudio.io.core.authenticator.datahandle;

import android.app.Activity;
import android.content.Intent;

import java.io.Serializable;

import mobilestudio.io.core.authenticator.model.User;

/**
 * Created by Sayed on 9/12/2017.
 */

public class Navigator implements Serializable {

    private Activity login;
    private Class register;
    private IDataHandle dataHandle;

    public Navigator(Activity login, Class<?> register) {
        this.login = login;
        this.register = register;
    }

    public void navigate(String UID) throws InterruptedException {
        User user =  dataHandle.getUserData();
        user.setUid(UID);
        Intent intent = new Intent(login, register);
        intent.putExtra("user",user);
        intent.putExtra("isSocial", true);
        login.startActivity(intent);
    }

    public void setDataHandler(IDataHandle dataHandle) {
        this.dataHandle = dataHandle;
    }


}
