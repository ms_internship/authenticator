package mobilestudio.io.core.authenticator.builder;

import android.app.Activity;
import android.content.Context;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import mobilestudio.io.core.authenticator.AuthenticationManager;
import mobilestudio.io.core.authenticator.OnAuthListener;
import mobilestudio.io.core.authenticator.authenticate.Authenticator;
import mobilestudio.io.core.authenticator.datahandle.IDBHandler;
import mobilestudio.io.core.authenticator.datahandle.ILinker;
import mobilestudio.io.core.authenticator.datahandle.UserDataProvider;
import mobilestudio.io.core.authenticator.model.User;
import mobilestudio.io.core.authenticator.signup.BaseSignUpAuth;
import mobilestudio.io.core.authenticator.signup.EmailAndPasswordSignUpAuth;
import mobilestudio.io.core.authenticator.signup.PhoneNumberSignUpAuth;
import mobilestudio.io.core.authenticator.signup.RegisterView;
import mobilestudio.io.core.authenticator.verification.Verifier;
import mobilestudio.io.core.authenticator.view.DefaultSignUpView;
import mobilestudio.io.core.authenticator.view.RegistrationViews;

/**
 * Created by pisoo on 9/8/2017.
 */

public class SignUpAuthBuilder {
    private BaseSignUpAuth EmailAndPasswordAuth , phoneNumberAuth;
    private Authenticator authenticator;
    private IDBHandler dbHandler ;
    private DefaultSignUpView signUpView;
    private User user;
    private Context context;
    private boolean onSocial ;

    public BaseSignUpAuth getEmailAndPasswordAuth() {
        return EmailAndPasswordAuth;
    }

    public BaseSignUpAuth getPhoneNumberAuth() {
        return phoneNumberAuth;
    }

    private RegistrationViews registrationViews = new RegistrationViews();
    private AuthenticationManager manager ;
    private ILinker linker ;
    private Verifier verifier ;
    private UserDataProvider userDataProvider ;





    public Context getContext() {
        return context;
    }


    public User getUser() {
        return user;
    }

    public AuthenticationManager getManager() {
        return manager;
    }

    public SignUpAuthBuilder(Authenticator authenticator , IDBHandler dbHandler  , Verifier verifier, OnAuthListener authListener) {
        this.authenticator = authenticator;
        userDataProvider = UserDataProvider.getInstance(dbHandler);
        userDataProvider.setRegistrationViews(registrationViews);
        manager = AuthenticationManager.getInstance(userDataProvider,dbHandler);
        manager.setListener(authListener);
        this.verifier = verifier ;
    }





    public SignUpAuthBuilder onEmailAndPasswordView(EditText email, EditText password, EditText confirmPassword, Button signUp, boolean isRequired) {
        EmailAndPasswordAuth = new EmailAndPasswordSignUpAuth(authenticator,manager,email, password, confirmPassword, signUp);
        registrationViews.email = new RegisterView<>();
        registrationViews.email.setView(email, isRequired);
        registrationViews.password = new RegisterView<>();
        registrationViews.password.setView(password, isRequired);
        registrationViews.confirmPassword = new RegisterView<>();
        registrationViews.confirmPassword.setView(confirmPassword, isRequired);
        registrationViews.signup = new RegisterView<>();
        registrationViews.signup.setView(signUp, isRequired);
        return this;
    }

    public SignUpAuthBuilder onDefaultEmailAndPasswordSignUP(boolean isRequired) {
        signUpView.setEmailVisible();
        signUpView.setPasswordVisible();
        signUpView.setConfrimasswordVisible();
        signUpView.setSignUVisible();
        return onEmailAndPasswordView(signUpView.getEmail(), signUpView.getPassword(), signUpView.getConfirmPassword(), signUpView.getSignup(), isRequired);
    }

    public SignUpAuthBuilder inflateDefaultView(Activity context, int id) {
        signUpView = context.findViewById(id);
        return this;
    }

    public boolean isOnSocial() {
        return onSocial;
    }

    public SignUpAuthBuilder setUserDataAtCompleteProfile(User user, boolean onSocial) {
        this.user = user;
        this.onSocial = onSocial;
        return this;
    }

    public SignUpAuthBuilder onPhoneView(EditText phone, boolean isRequired , boolean isAuthenticated) {

         registrationViews.phone = new RegisterView<>();
         registrationViews.phone.setView(phone,(isAuthenticated || isRequired));
        if(isAuthenticated){
                phoneNumberAuth = new PhoneNumberSignUpAuth(authenticator,manager,verifier,registrationViews.phone.view);
            }


        return this;
    }

    public SignUpAuthBuilder onDefaultPhoneView(boolean isRequired, boolean isAuthenticated) {
        signUpView.setPhoneVisible();
        return onPhoneView(signUpView.getPhone(), isRequired,isAuthenticated);
    }

    public SignUpAuthBuilder onNameView(EditText firstName, boolean isRequired) {
        registrationViews.firstName = new RegisterView<>();
        registrationViews.firstName.setView(firstName, isRequired);
        return this;
    }

    public SignUpAuthBuilder onDefaultNameView(boolean isRequired) {
        signUpView.setFirstNameVisible();
        return onNameView(signUpView.getFirstName(), isRequired);
    }

    public SignUpAuthBuilder onFirstAndLastNameView(EditText firstName, EditText lastName, boolean isRequired) {
        registrationViews.firstName = new RegisterView<>();
        registrationViews.firstName.setView(firstName, isRequired);
        registrationViews.lastName = new RegisterView<>();
        registrationViews.lastName.setView(lastName, isRequired);
        return this;
    }

    public SignUpAuthBuilder onDefaultFirstAndLastNameView(boolean isRequired) {
        signUpView.setFirstNameVisible();
        signUpView.setLastNameVisible();
        return onFirstAndLastNameView(signUpView.getFirstName(), signUpView.getLastName(), isRequired);
    }

    public SignUpAuthBuilder onBirthDateView(EditText birthDate, boolean isRequired, Context context) {
        this.context = context;
        registrationViews.birthdate.setView(birthDate, isRequired);
        return this;
    }

    public SignUpAuthBuilder onDefaultBirthDateView(boolean isRequired, Context context) {

        signUpView.setBirthdateVisible();
        return onBirthDateView(signUpView.getBirthdate(), isRequired, context);
    }

    public SignUpAuthController build() {
        return new SignUpAuthController(this);
    }

    public SignUpAuthBuilder onDefaultGenderView(boolean isRequired) {
        signUpView.setGenderVisible();
        return onGenderView(signUpView.getGender(), isRequired);
    }

    public SignUpAuthBuilder onGenderView(Spinner gender, boolean isRequired) {
        registrationViews.gender.setView(gender, isRequired);
        return this;
    }

    public DefaultSignUpView getSignUpView() {
        return signUpView;
    }

    public RegistrationViews getRegistrationViews() {
        return registrationViews;
    }

    public SignUpAuthBuilder setLinker(ILinker linker){
        this.linker = linker;
        return  this;
    }

    public ILinker getLinker() {
        return linker;
    }
}
