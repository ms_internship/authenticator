package mobilestudio.io.core.authenticator.datahandle;

/**
 * Created by Sayed on 9/18/2017.
 */

public interface OnLinkCallback {

    void onSuccess(String uid);

    void onFailed(String errorMessage);
}
