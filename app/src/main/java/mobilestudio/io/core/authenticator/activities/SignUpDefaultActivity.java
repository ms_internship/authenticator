package mobilestudio.io.core.authenticator.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import mobilestudio.io.core.authenticator.OnAuthListener;
import mobilestudio.io.core.authenticator.R;
import mobilestudio.io.core.authenticator.authenticate.FirebaseAuthenticator;
import mobilestudio.io.core.authenticator.builder.SignUpAuthBuilder;
import mobilestudio.io.core.authenticator.builder.SignUpAuthController;
import mobilestudio.io.core.authenticator.datahandle.FirebaseDBHandler;
import mobilestudio.io.core.authenticator.model.User;
import mobilestudio.io.core.authenticator.verification.FirebasePhoneNumberVerifier;

public class SignUpDefaultActivity extends AppCompatActivity implements OnAuthListener {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_default);
        User user = new User();
        if (getIntent().getExtras() != null) {
            user = (User) getIntent().getExtras().get("user");
        } else {
            user.setEmail("MustafaPiso@yahoo.com");
            user.setFirstName("Mustafa ");
            user.setLastName("Gamal");
            user.setBirthDate("11/10/1998");
            user.setGender("Male");
        }

        SignUpAuthController controller = new SignUpAuthBuilder(new FirebaseAuthenticator(), new FirebaseDBHandler("UsersProfile"),
                new FirebasePhoneNumberVerifier(this),this)
                .inflateDefaultView(this, R.id.defaultView)
                .onDefaultEmailAndPasswordSignUP(true)
                .onDefaultBirthDateView(true, this)
                .onDefaultFirstAndLastNameView(false)
                .setUserDataAtCompleteProfile(user, getIntent().getBooleanExtra("isSocial",false))
                .build();
    }

    @Override
    public void onSuccess(User user) {
        Toast.makeText(this, " i've user now ", Toast.LENGTH_SHORT).show();
        startActivity( new Intent(this , MainActivity.class));

    }

    @Override
    public void onFailed(String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
    }
}
