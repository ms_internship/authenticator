package mobilestudio.io.core.authenticator.builder;

import android.content.Intent;

import mobilestudio.io.core.authenticator.login.FacebookLoginAuth;
import mobilestudio.io.core.authenticator.login.GmailLoginAuth;

/**
 * Created by pisoo on 9/8/2017.
 */

public class LoginAuthController {

    private LoginAuthBuilder builder;

    public LoginAuthController(LoginAuthBuilder builder) {
        this.builder = builder;
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        builder.onActivityResult(requestCode, resultCode, data);
    }
}
