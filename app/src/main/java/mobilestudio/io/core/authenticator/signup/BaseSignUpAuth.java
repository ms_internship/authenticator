package mobilestudio.io.core.authenticator.signup;

import mobilestudio.io.core.authenticator.authenticate.Authenticator;

/**
 * Created by pisoo on 9/8/2017.
 */

public abstract class BaseSignUpAuth {
    Authenticator authenticator;

    public BaseSignUpAuth(Authenticator authenticator) {
        this.authenticator = authenticator;
    }

    public abstract void signUp(onSignUpCallback callback);
}
