package mobilestudio.io.core.authenticator.authenticate;

/**
 * Created by Sayed on 9/6/2017.
 */

public interface OnAuthenticateCallback {

    void onSuccess(String UID);

    void onFailed(String errorMessage);

}
