package mobilestudio.io.core.authenticator.login;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.firebase.auth.FirebaseAuth;

import java.util.Arrays;

import mobilestudio.io.core.authenticator.AuthenticationManager;
import mobilestudio.io.core.authenticator.authenticate.Authenticator;
import mobilestudio.io.core.authenticator.authenticate.OnAuthenticateCallback;
import mobilestudio.io.core.authenticator.datahandle.FacebookDataHandle;
import mobilestudio.io.core.authenticator.datahandle.Navigator;

/**
 * Created by Sayed on 9/6/2017.
 */

public class FacebookLoginAuth extends LoginAuthCallback  {
    private CallbackManager callbackManager;
    private Button loginButton;
     private AuthenticationManager manager;

    public FacebookLoginAuth(final Authenticator authenticator , final AuthenticationManager manager, final Navigator firstTimeHandler, final Button loginButton, Activity activity) {
        super(authenticator, activity, firstTimeHandler);

        this.loginButton = loginButton;
        this.manager = manager;

        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().registerCallback(
                callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(final LoginResult loginResult) {
                        firstTimeHandler.setDataHandler( new FacebookDataHandle(loginResult.getAccessToken()));
                        manager.setNavigator(firstTimeHandler);
                        authenticator.loginWithFacebook(loginResult.getAccessToken(), new OnAuthenticateCallback() {
                            @Override
                            public void onSuccess(String UID) {
                                manager.onSocialSuccess(UID);
                            }

                            @Override
                            public void onFailed(String errorMessage) {
                                manager.onFail(errorMessage);
                            }
                        });

                    }
                    @Override
                    public void onCancel() {

                        Log.v("FBmessage ", "cancel");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Log.v("FBmessage ", exception.getMessage());
                    }
                }
        );

        loginButton.setOnClickListener(new View.OnClickListener()

        {
            @Override
            public void onClick(View v) {
                login();
            }
        });
    }


    @Override
    public void login() {
        if(FirebaseAuth.getInstance().getCurrentUser() != null )
        FirebaseAuth.getInstance().signOut();
        LoginManager.getInstance().logInWithReadPermissions(
                getActivity(),
                Arrays.asList("user_photos", "email", "user_birthday", "public_profile")
        );

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


}
