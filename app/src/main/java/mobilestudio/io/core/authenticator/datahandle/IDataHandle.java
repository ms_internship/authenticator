package mobilestudio.io.core.authenticator.datahandle;

import mobilestudio.io.core.authenticator.model.User;

/**
 * Created by Sayed on 9/12/2017.
 */

public interface IDataHandle {

    User getUserData() throws InterruptedException;
}
