package mobilestudio.io.core.authenticator.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import mobilestudio.io.core.authenticator.OnAuthListener;
import mobilestudio.io.core.authenticator.builder.LoginAuthBuilder;
import mobilestudio.io.core.authenticator.builder.LoginAuthController;
import mobilestudio.io.core.authenticator.R;
import mobilestudio.io.core.authenticator.authenticate.FirebaseAuthenticator;
import mobilestudio.io.core.authenticator.authenticate.OnAuthenticateCallback;
import mobilestudio.io.core.authenticator.datahandle.FirebaseDBHandler;
import mobilestudio.io.core.authenticator.model.User;

public class LoginDefaultActivity extends AppCompatActivity implements OnAuthListener {
    LoginAuthController controller;
    String gmailClientID = "454171667567-qk8slqn1cqahqll39vgj5aff5e3pu6pi.apps.googleusercontent.com";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_default);
        controller = new LoginAuthBuilder(new FirebaseAuthenticator()
                                         ,new FirebaseDBHandler("UserProfile"),this,
                                         this ,SignUpDefaultActivity.class)
                .inflateDefaultView(this, R.id.defaultView)
                .onDefaultEmailAndPasswordLogin()
                .onDefaultGmailLogin(gmailClientID)
                .onDefaultPhoneNumberLogin()
                .onDefaultFacebookLogin()
                .build();
    }

    @Override
    public void onSuccess(User user) {
        Toast.makeText(this,"I'm getting the user now ", Toast.LENGTH_SHORT).show();
         Log.v("User " , user.getFirstName() +" "+ user.getLastName() + " with Id "+ user.getUid());
         Intent intent = new Intent(this , MainActivity.class) ;
         intent.putExtra("user" , user.getFirstName() +" "+ user.getLastName() + " with Id "+ user.getUid() ) ;
         startActivity(intent);
    }

    @Override
    public void onFailed(String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        controller.onActivityResult(requestCode, resultCode, data);
    }
}
