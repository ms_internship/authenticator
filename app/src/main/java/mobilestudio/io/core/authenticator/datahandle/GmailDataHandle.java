package mobilestudio.io.core.authenticator.datahandle;

import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.firebase.auth.FirebaseAuth;

import mobilestudio.io.core.authenticator.model.User;

/**
 * Created by Sayed on 9/13/2017.
 */

public class GmailDataHandle implements IDataHandle {
    GoogleSignInAccount googleSignInAccount;
    public GmailDataHandle(GoogleSignInAccount googleSignInAccount ){
        this.googleSignInAccount = googleSignInAccount;
    }
    @Override
    public User getUserData() {
        User user  = new User();
        user.setEmail(googleSignInAccount.getEmail());
        user.setFirstName(googleSignInAccount.getDisplayName());
        user.setLastName(googleSignInAccount.getFamilyName());
        return user;
    }
}
