package mobilestudio.io.core.authenticator;

import mobilestudio.io.core.authenticator.datahandle.FirstTimeCallback;
import mobilestudio.io.core.authenticator.datahandle.IDBHandler;
import mobilestudio.io.core.authenticator.datahandle.Navigator;
import mobilestudio.io.core.authenticator.datahandle.OnDataFetched;
import mobilestudio.io.core.authenticator.datahandle.UserDataProvider;
import mobilestudio.io.core.authenticator.model.User;


/**
 * Created by pisoo on 9/17/2017.
 */

public class AuthenticationManager {
    private static AuthenticationManager manager;

    private OnAuthListener listener;
    private boolean onSocial;
    private Navigator navigator;
    private IDBHandler databaseHandler;
    private UserDataProvider dataProvider;

    public void setNavigator(Navigator navigator) {
        this.navigator = navigator;
    }

    public void setListener(OnAuthListener listener) {
        this.listener = listener;
    }

    public void setOnSocial(boolean onSocial) {
        this.onSocial = onSocial;
    }

    private AuthenticationManager(UserDataProvider dataProvider, IDBHandler handler) {
        this.databaseHandler = handler;
        this.dataProvider = dataProvider;
    }

    public static AuthenticationManager getInstance(UserDataProvider provider, IDBHandler dbHandler) {
        if (manager == null) {
            manager = new AuthenticationManager(provider, dbHandler);
        }
        return manager;
    }

    public void onSocialSuccess(final String UID) {
        databaseHandler.checkFirstTime(UID, new FirstTimeCallback() {
            @Override
            public void isFirstTime(boolean firstTime) throws InterruptedException {
                if (firstTime && onSocial) {
                    // navigate to the Activity and send the User data
                    navigator.navigate(UID);
                } else {
                    dataProvider.loadUserFromDB(UID, new OnDataFetched() {
                        @Override
                        public void onSuccess(User user) {
                            listener.onSuccess(user);
                        }
                    });
                }
            }
        });

    }
    public void onSuccessNormal(String UID ) {
        User user = dataProvider.getUserLocal();
        user.setUid(UID);
        listener.onSuccess(user);
        databaseHandler.setUser(user);
    }

    public void onFail(String errorMessage) {
        listener.onFailed(errorMessage);
    }
}
