package mobilestudio.io.core.authenticator.activities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.facebook.login.widget.LoginButton;
import com.google.android.gms.common.SignInButton;

import mobilestudio.io.core.authenticator.OnAuthListener;
import mobilestudio.io.core.authenticator.builder.LoginAuthBuilder;
import mobilestudio.io.core.authenticator.R;
import mobilestudio.io.core.authenticator.authenticate.FirebaseAuthenticator;
import mobilestudio.io.core.authenticator.authenticate.OnAuthenticateCallback;
import mobilestudio.io.core.authenticator.builder.LoginAuthController;
import mobilestudio.io.core.authenticator.datahandle.FirebaseDBHandler;
import mobilestudio.io.core.authenticator.model.User;

public class LoginCustomActivity extends AppCompatActivity implements OnAuthListener {
    EditText email , password ,phoneNumber  ;
    Button loginButton , sendCode  ;
    String gmailClientID = "454171667567-qk8slqn1cqahqll39vgj5aff5e3pu6pi.apps.googleusercontent.com" ;
    SignInButton gmail ;
    LoginButton facebook ;
    LoginAuthController controller ;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login_custom);
        email = (EditText) findViewById(R.id.ed_EmailAddress);
        password = (EditText) findViewById(R.id.ed_password);
        loginButton = (Button) findViewById(R.id.bt_Login);
        sendCode = (Button) findViewById(R.id.bt_sentVerificationNumber);
        phoneNumber = (EditText) findViewById(R.id.ed_phoneNumber);
        facebook = (LoginButton) findViewById(R.id.login_button );
        gmail = (SignInButton) findViewById(R.id.sign_in_button);
        controller = new LoginAuthBuilder(new FirebaseAuthenticator(),new FirebaseDBHandler("userProfile"),this,this,SignUpDefaultActivity.class)
                .onEmailAndPasswordLogin(email, password, loginButton)
                .onPhoneNumber(phoneNumber,sendCode)
                .onFacebookLogin(facebook)
                .build();
    }


    @Override
    public void onSuccess(User user) {
        Toast.makeText(this,"I've User now ", Toast.LENGTH_SHORT).show();
        startActivity( new Intent(this , MainActivity.class));
    }

    @Override
    public void onFailed(String errorMessage) {
        Toast.makeText(this, errorMessage, Toast.LENGTH_SHORT).show();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        controller.onActivityResult(requestCode, resultCode, data);
    }
}
